#ifndef __SUDO_H_
#define __SUDO_H_

#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 9

const int KEYS[MAX_SIZE] = {1,2,3,4,5,6,7,8,9};

typedef struct sudo_use{
	int available[MAX_SIZE];
	int avai_index;
	int unavailable[MAX_SIZE];
	int unavai_index;
}sudo_use_t;

typedef struct sudo_key{
	int value;
	sudo_use_t is_use;
}sudo_key_t;

typedef struct sudo_site{
	int site_x;
	int site_y;
	int value;
}sudo_site_t;





#ifdef __cplusplus
extern "C"{
#endif

	void init_sudo_map();

	int make_sudo_map();

	void set_sudo_map(sudo_site_t* site);

#ifdef __cplusplus
}
#endif
#endif
