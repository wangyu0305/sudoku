#include "sudo.h"

sudo_key_t SUDO_MAP[MAX_SIZE][MAX_SIZE];

extern sudo_site_t site_value[MAX_SIZE*MAX_SIZE];

void init_sudo_map()
{
	for(int x = 0;x < MAX_SIZE;x++)
	{
		for(int y = 0;y < MAX_SIZE;y++)
		{
			SUDO_MAP[x][y].value = 0;
			for(int i = 0;i < MAX_SIZE;i++)
			{
				SUDO_MAP[x][y].is_use.available[i] = KEYS[i];
				SUDO_MAP[x][y].is_use.unavailable[i] = 0;
			}
			SUDO_MAP[x][y].is_use.avai_index = MAX_SIZE;
			SUDO_MAP[x][y].is_use.unavai_index = 0;
		}
	}
}

void set_sudo_map(sudo_site_t* site)
{
	while(site->value != -1)
	{
		SUDO_MAP[site->site_x][site->site_y].value = site->value;
		site++;
	}
}

void less_aval_key(sudo_use_t* use_key,int value)
{
	for(int x = 0;x < use_key->avai_index;x++)
	{
		if(use_key->available[x] == value)
		{
			for(int y = x + 1;y < use_key->avai_index;y++)
			{
				use_key->available[y-1] = use_key->available[y];
			}
			use_key->available[use_key->avai_index-1] = 0;
			use_key->avai_index--;
			break;
		}
	}
}

int check_unaval_exist(sudo_use_t* use_key,int value)
{
	for(int x = 0;x < use_key->unavai_index;x++)
	{
		if(use_key->unavailable[x] == value)
			return 1;
	}
	return 0;
}

void process_unaval_key(int x,int y,sudo_key_t* signalkey)
{
	if(0 == check_unaval_exist(&signalkey->is_use, SUDO_MAP[x][y].value))
	{
		signalkey->is_use.unavailable[signalkey->is_use.unavai_index] = SUDO_MAP[x][y].value;
		signalkey->is_use.unavai_index++;
		less_aval_key(&signalkey->is_use, SUDO_MAP[x][y].value);
	}
}

void check_key(int site_x,int site_y,sudo_key_t* signalkey)
{
	for(int x = 0;x < site_x;x++)
	{
		int y = site_y;
		process_unaval_key(x,y,signalkey);
	}
	for(int y = 0;y < site_y;y++)
	{
		int x = site_x;
		process_unaval_key(x,y,signalkey);
	}
}

int aval_key(int site_x,int site_y,sudo_key_t* signalkey)
{
	check_key(site_x,site_y,signalkey);
	if(0 == signalkey->is_use.avai_index)
		return 0;
	int index = random()%signalkey->is_use.avai_index;
	return signalkey->is_use.available[index];
}

int make_sudo_map()
{
	for(int x=0;x < MAX_SIZE;x++)
	{
		for(int y=0;y < MAX_SIZE;y++)
		{
			if(SUDO_MAP[x][y].value == 0){
				int key = aval_key(x,y,&SUDO_MAP[x][y]);
				if(key == 0)
					return 0;
				SUDO_MAP[x][y].value = key;
			}

		}
	}
	return 1;
}


