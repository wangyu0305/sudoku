/*author wangyu*/
#include <string.h>
#include "sudo.h"

sudo_site_t site_value[MAX_SIZE*MAX_SIZE];

extern sudo_key_t SUDO_MAP[MAX_SIZE][MAX_SIZE];

void loop_init()
{
	while(true)
	{
		init_sudo_map();
		if(1 == make_sudo_map())
			break;
	}
}

void loop_set()
{
	while(true)
	{
		init_sudo_map();
		set_sudo_map(site_value);
		if(1 == make_sudo_map())
				break;
	}
}

void parse_site(sudo_site_t* site,char* in_x, char* in_y,char* in_v)
{
	int tmp[3];
	char* value[3];
	int x = 0;
	int sum = 0;
	char c;
	value[0] = in_x;
	value[1] = in_y;
	value[2] = in_v;
	for(int i = 0;i < 3;i++)
	{
		while((c = *value[i]) != '\0'){
				sum += sum*10 + (c - '0');
				value[i]++;
			}
			tmp[x] = sum;
			sum = 0;
			x++;
	}
	site->site_x = tmp[0];
	site->site_y = tmp[1];
	site->value = tmp[2];
}

void sudo_set_array()
{
//	memset(site_value,0,sizeof(site_value));
	for(int i = 0;i < MAX_SIZE*MAX_SIZE;i++){
		site_value[i].site_x = 0;
		site_value[i].site_y = 0;
		site_value[i].value = -1;
	}
	int i = 0;
	while(true)
	{
		char* input[3];
		input[0] = (char*)malloc(sizeof(char)*9);
		input[1] = (char*)malloc(sizeof(char)*9);
		input[2] = (char*)malloc(sizeof(char)*9);
		scanf("%s%s%s",input[0],input[1],input[2]);
		if(strcmp("end", input[0]) == 0)
		{
			break;
		}
		else{
			parse_site(&site_value[i], input[0],input[1],input[2]);
			i++;
		}
		free(input[0]);
		free(input[1]);
		free(input[2]);
	}
}

void usage()
{
	printf("usage: \n");
	printf("cmd: s means start --start a sudoku\n");
	printf("cmd: i means init --make a new sudoku\n");
	printf("x-site y-site value\n");
}

void get_args()
{
	char cmd;
	scanf("%c", &cmd);
	switch(cmd)
	{
	case 'i':
		loop_init();
		break;
	case 's':
		sudo_set_array();
		break;
	default:
		usage();
		break;
	}
}

int main(int argc,char** argv)
{
	get_args();
	for(int i = 0;i < MAX_SIZE;i++)
	{
		for(int j = 0;j < MAX_SIZE;j++)
		{
			printf(" %d ",SUDO_MAP[i][j].value);
		}
		printf("\n");
	}
}
